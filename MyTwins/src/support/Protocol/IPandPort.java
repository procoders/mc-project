package support.Protocol;

public class IPandPort {
	
	private static int[] port = {9990,9991,9992,9993,9994,9995 };
	private static String mobIP = "192.168.58.57";
	private static String desktopIP = "192.168.56.121";//48.191"; 
	public static int getPort(int i) {
		return port[i];
	}
	public static String getMobIP() {
		return mobIP;
	}
	public static String getDesktopIP() {
		return desktopIP;
	}

}
