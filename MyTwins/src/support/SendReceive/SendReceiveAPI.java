package support.SendReceive;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import support.Metadata.MetaDataList;
import support.Protocol.IPandPort;
import support.Protocol.Signal;

public class SendReceiveAPI 
{
	Server server=null;
	private static boolean Running = true;
	
	private int receivePort=IPandPort.getPort(4);
	private String receiveIP=IPandPort.getDesktopIP();
	
	public SendReceiveAPI(Server server)
	{
		this.server=server;
	}
	
	/**
	 * 
	 * @param mode str -> string, l -> List of MetaData, f--> mp3 file , i --> Integer, sig--> signal
	 * @return 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	 public Object receiverDemoText(String mode)
	 { 
		printMessage("Receiver", "Receiver Start");
		
		
		Object received = null;
		//if (true) 
		{
			try
			{
				Running = false;
				SocketChannel sChannel = SocketChannel.open();
				sChannel.configureBlocking(true);
				sChannel.connect(new InetSocketAddress(receiveIP, receivePort));
				ObjectInputStream ois = new ObjectInputStream(sChannel.socket().getInputStream());
				Running = true;
				if(mode.equals("str"))
				{
					received = (String)ois.readObject();	    		
				}
				else if(mode.equals("l"))
				{
					received = (MetaDataList)ois.readObject();	
				}
				else if(mode.equals("f"))
				{
					received = (File)ois.readObject();	
				}
				else if(mode.equals("i"))
				{
					received = (Integer)ois.readObject();	
				}
				else if(mode.equals("sig"))
				{
					received = (Signal)ois.readObject();	
				}
				else
				{
					printMessage("Receiver","Invalid mode");
				}
				printMessage("Receiver", "String is: '" + received + "'");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			finally
			{
				Running = true;
			}
		}
		printMessage("Receiver", "End Receiver");
		return received;
	 }
	 
	 public void senderDemoText(Object obj)
	 {
		printMessage("Sender","Sender Start");
		printMessage("Sender", "String is: '" + obj + "'");
		ObjectOutputStream  oos=null;
		SocketChannel sChannel=null;
		try
		{
			sChannel = server.getSsChannel().accept(); //blocking
			
			oos = new 
		              ObjectOutputStream(sChannel.socket().getOutputStream());
			oos.writeObject(obj);
			
			printMessage("Sender","Sender Sent Data");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			printMessage("Sender","Sender Did not Send Data");
		}
		finally
		{
			try {oos.close();} catch (IOException e) {e.printStackTrace();}
			//finally{try {sChannel.close();} catch (IOException e) {e.printStackTrace();}}
		}
	 }
	 
	 void printMessage(String tag,String msg)
	 {
		 //Log.d(tag, msg);
		 System.out.println(tag+" : "+msg);
	 }

	public static boolean isRunning() {
		return Running;
	}
}
