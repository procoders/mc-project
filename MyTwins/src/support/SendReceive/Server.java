package support.SendReceive;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import support.Protocol.IPandPort;

public class Server 
{
	//sender is server
	private int sendPort=IPandPort.getPort(5);
	private int receivePort=IPandPort.getPort(4);
	private String receiveIP=IPandPort.getDesktopIP();
	private SocketChannel sChannel=null;
	private ServerSocketChannel ssChannel=null;
	
	public Server()
	{
		try
		{
			restartServer();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void sendStartup() throws IOException
	{
		ssChannel = ServerSocketChannel.open();
		ssChannel.configureBlocking(true);
		ssChannel.socket().bind(new InetSocketAddress(sendPort));
	}
	
	private void receiveStartup() throws IOException
	{
		//System.out.println("rh1");
		//sChannel = SocketChannel.open();
		//sChannel.configureBlocking(true);
		//sChannel.connect(new InetSocketAddress(receiveIP, receivePort));
	}
	
	public void restartServer() throws IOException
	{
		if(sChannel!=null)
		if(sChannel.isOpen())
		{
			sChannel.close();
		}
		
		if(ssChannel!=null)
		if(ssChannel.isOpen())
		{
			ssChannel.close();
		}
		
		sendStartup();
		receiveStartup();
	}

	public void restartReceiver() throws IOException
	{
		if(sChannel.isOpen())
		{
			sChannel.close();
		}
		
		receiveStartup();
	}
	
	public void closeServer() 
	{
		if(sChannel!=null)
		if(sChannel.isOpen())
		{
			try{sChannel.close();}catch(Exception e){e.printStackTrace();}
		}
		if(ssChannel!=null)
		if(ssChannel.isOpen())
		{
			try{ssChannel.close();}catch(Exception e){e.printStackTrace();}
		}
		

		sChannel=null;
		ssChannel=null;
	}
	
	public int getReceivePort() {
		return receivePort;
	}

	public void setReceivePort(int receivePort) {
		this.receivePort = receivePort;
	}

	public String getReceiveIP() {
		return receiveIP;
	}

	public void setReceiveIP(String receiveIP) {
		this.receiveIP = receiveIP;
	}

	public SocketChannel getsChannel() {
		return sChannel;
	}

	public void setsChannel(SocketChannel sChannel) {
		this.sChannel = sChannel;
	}

	public int getSendPort() {
		return sendPort;
	}

	public ServerSocketChannel getSsChannel() {
		return ssChannel;
	}

}
