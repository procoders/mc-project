package support.Metadata;

import java.io.Serializable;
import java.util.Date;

public class MetaData implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String name;
	long size;
	Date ModifiedOn;
	String path;
	
	public MetaData(String name, long size, Date ModifiedOn, String path) 
	{
		super();
		this.name = name;
		this.size = size;
		this.ModifiedOn = ModifiedOn;
		this.path=path;
	}
	
	//getters
	public String getName() {
		return name;
	}
	public float getSize() {
		return size;
	}
	public Date getCreatedOn() {
		return ModifiedOn;
	}
	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		return "MetaData [name=" + name + ", size=" + size + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (size ^ (size >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MetaData other = (MetaData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (size != other.size)
			return false;
		return true;
	}

}
