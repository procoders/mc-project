package support.Metadata;

import java.io.Serializable;
import java.util.ArrayList;

public class MetaDataList implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ArrayList<MetaData> list=new ArrayList<MetaData>();
	
	public String toString()
	{
		String retval=new String();
		int i=0;
		for(MetaData meta:list)
		{
			retval+="**"+(++i)+"**"+meta.toString()+"\n";
		}
		return retval;
	}
	
	public int diff(MetaDataList otherList)
	{
		int retval=0;
		ArrayList<MetaData> smallList=null;
		ArrayList<MetaData> bigList=null;
		
		if(this.list.size()<otherList.list.size())
		{
			smallList=this.list;
			bigList=otherList.list;
		}
		else
		{
			smallList=otherList.list;
			bigList=this.list;
		}
		
		for(int i=smallList.size()-1;i>=0;i--)
		{
			if(bigList.contains(smallList.get(i)))
			{
				
				bigList.remove(smallList.get(i));
				smallList.remove(i);
				retval+=1;
			}
		}
		return retval;
		
	}

}
