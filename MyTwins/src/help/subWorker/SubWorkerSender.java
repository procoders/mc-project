package help.subWorker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import support.Metadata.MetaData;
import support.Metadata.MetaDataList;
import support.Protocol.IPandPort;
import support.Protocol.Signal;
import support.SendReceive.SendReceiveAPI;
import android.util.Log;

public class SubWorkerSender 
{
	private Socket client;
	private FileInputStream fileInputStream;
	private BufferedInputStream bufferedInputStream;
	private OutputStream outputStream;
	private boolean clientRunning=false;
	
	private int receivePort=IPandPort.getPort(3);
	private String receiveIP=IPandPort.getDesktopIP();
	
	public int sendMusicFiles(MetaDataList metaDataList, SendReceiveAPI sendReceiveAPI) 
	{
		for(MetaData metadata:metaDataList.list)
	    {	
			while(true)
			{
				if(sendReceiveAPI.receiverDemoText("sig")==Signal.RECEIVER_RESTARTED)
				{
					break;
				}	
			}
	    	//if(mp3.getName().matches(".*\\.mp3$"))
	    	{
	    		//Log.d("extension",l.getName());
	    		try
	    		{
	    			 System.out.println(metadata.getPath());
	    			 
	    			 File mp3=new File(metadata.getPath());
		   		     startClient();
	    			 Log.d("SIZE", ""+mp3.length() );
		   		     byte[] mybytearray = new byte[(int) mp3.length()]; //create a byte array to file
		   		     
		   		     fileInputStream = new FileInputStream(mp3);
		   		     bufferedInputStream = new BufferedInputStream(fileInputStream);  
		   		     
		   		     bufferedInputStream.read(mybytearray, 0, mybytearray.length); //read the file
		   		     
		   		     outputStream = client.getOutputStream();
		   		     
		   		     outputStream.write(toByteArray((int) mp3.length()));//send length
		   		     
		   		     outputStream.write(mybytearray, 0, mybytearray.length); //write file to the output stream byte by byte
		   		     
		   		     outputStream.flush();
		   		     
		   		     bufferedInputStream.close();
		   		     outputStream.close();
		   		     
		   		     closeClient();
	    			
	    		}
	    		catch (UnknownHostException e) 
	    		{
		    		e.printStackTrace();
		    		return 0;
		    	} 
	    		catch (IOException e) 
	    		{
		    		e.printStackTrace();
		    		return 0;
		    	}
	    		
	    	}
	    	
	    }    
	    	return 1;
	}

	private void closeClient() throws IOException {
		if(clientRunning==true)
		{
			clientRunning=false;
			client.close();
		}
		
	}

	private void startClient() throws UnknownHostException, IOException 
	{
		if(clientRunning==false)
		{
			clientRunning=true;
			client = new Socket(receiveIP, receivePort);
		}
		
	}
	
	static byte[] toByteArray(int value) {
	    return new byte[] {
	        (byte) (value >> 24),
	        (byte) (value >> 16),
	        (byte) (value >> 8),
	        (byte) value};
	}
	
	static int fromByteArray(byte[] bytes) {
	     return bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
	}

}
