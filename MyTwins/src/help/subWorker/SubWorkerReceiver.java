package help.subWorker;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import support.Metadata.MetaData;
import support.Metadata.MetaDataList;
import support.Protocol.IPandPort;
import support.Protocol.Signal;
import support.SendReceive.SendReceiveAPI;
import android.os.Environment;

public class SubWorkerReceiver
{
	private static ServerSocket serverSocket;
    private static Socket clientSocket;
    private static InputStream inputStream;
    private static FileOutputStream fileOutputStream;
    private static BufferedOutputStream bufferedOutputStream;
    
    private static int bytesRead;
    private static int current = 0;
    
    private static int filesize = 0; // filesize temporary hardcoded 
    
    private static int serverPort= IPandPort.getPort(2);
    
    
    public static void startReceiver(MetaDataList metaDataList, SendReceiveAPI sendReceiveAPI) throws IOException {
        
        
        System.out.println("Server started. Listening to the port 4444");
        
    	
        for(MetaData m:metaDataList.list)
        {
        	
			sendReceiveAPI.senderDemoText(Signal.RECEIVER_RESTARTED);
			
        	serverSocket = new ServerSocket(serverPort);  //Server socket
        	System.out.println("Waiting for client");
        	clientSocket = serverSocket.accept();
        	getFile(m.getName());
        }
        
    }
    
    private static void getFile(String fileName) throws IOException
    {
    	
    	byte[] fileSizeByte=new byte[4];
		
    	inputStream = clientSocket.getInputStream();
    	
    	while((fileSizeByte[0]=(byte) inputStream.read())>=0)
    	{
    		
    		for(int i=1;i<4;i++)
			{
				fileSizeByte[i]=(byte) inputStream.read();
				//System.out.println(""+inputStream.read());
			}
	    	
	    	filesize=fromByteArray(fileSizeByte);
	    	//System.out.println(filesize);
			
	        byte[] mybytearray = new byte[filesize];    //create byte array to buffer the file
	 
	        fileOutputStream = new FileOutputStream(getSyncDir()+"//"+fileName);
	        System.out.println("SyncDir " + getSyncDir()+"//"+fileName);
	        bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
	 
	        System.out.println("Receiving "+fileName+"...");
	        
	      //following lines read the input slide file byte by byte
	        bytesRead = inputStream.read(mybytearray, 0, filesize);
	        current = bytesRead;
	 
	        //for(int i=1;i<filesize;i++)
	        do
	        {
	            bytesRead = inputStream.read(mybytearray, current, (filesize-current));
	            if (bytesRead > 0) {
	                current += bytesRead;
	            }
	            if(current==filesize)
	            {
	            	break;
	            }
	            //System.out.println(current+"\t : "+mybytearray[current-1]);
	        } while (bytesRead > -1);
	        
	        bufferedOutputStream.write(mybytearray, 0, current);
	        bufferedOutputStream.flush();
	        bufferedOutputStream.close();
    	}
        
        
        inputStream.close();
        clientSocket.close();
        serverSocket.close();
 
        System.out.println("Server recieved file : "+fileName);
    }
    
    static byte[] toByteArray(int value) {
	    return new byte[] {
	        (byte) (value >> 24),
	        (byte) (value >> 16),
	        (byte) (value >> 8),
	        (byte) value};
	}
	
	static int fromByteArray(byte[] bytes) {
	     return bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
	}
	
	private static  File getSyncDir() 
	{
		String root_sd = Environment.getExternalStorageDirectory().toString();
	    return new File( root_sd + "/downloads/bluetooth" ) ;
	    
	}

}
