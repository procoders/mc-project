package com.mobilemafia.mytwins.database;

public class Profile {
	private String ssidString;
	private String ipString;
	private String profileString;
	private String syncpathString;
	private String portString;
	
	public String getSsidString() {
		return ssidString;
	}
	public void setSsidString(String ssidString) {
		this.ssidString = ssidString;
	}
	public String getIpString() {
		return ipString;
	}
	public void setIpString(String ipString) {
		this.ipString = ipString;
	}
	public String getProfileString() {
		return profileString;
	}
	public void setProfileString(String profileString) {
		this.profileString = profileString;
	}
	public String getSyncpathString() {
		return syncpathString;
	}
	public void setSyncpathString(String syncpathString) {
		this.syncpathString = syncpathString;
	}
	public String getPortString() {
		return portString;
	}
	public void setPortString(String portString) {
		this.portString = portString;
	}

}