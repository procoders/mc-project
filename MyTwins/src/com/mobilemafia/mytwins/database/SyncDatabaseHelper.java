package com.mobilemafia.mytwins.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SyncDatabaseHelper {

	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_NAME = "syncedin.db";
	private static final String TABLE_NAME_PROFILE = "profilerecords";
	private static final String TABLE_NAME_DELETED = "deletedrecords";
	private static final String TABLE_NAME_SYNCED = "syncedrecords";
	
	private static final String Profile_COLUMN_SSID = "ssid";
	private static final String Profile_COLUMN_IP = "ip";
	private static final String Profile_COLUMN_PROFILE = "profile";
	private static final String Profile_COLUMN_SYNCPATH = "syncpath";
	private static final String Profile_COLUMN_PORT = "port";
	private static final String SYNCED_COLUMN_TIMESTAMP = "timestamp";
	private static final String SYNCED_COLUMN_NAME = "name";
	private static final String SYNCED_COLUMN_SIZE = "size";
	private static final String DELETED_COLUMN_TIMESTAMP = "timestamp";
	private static final String DELETED_COLUMN_NAME = "name";
	private static final String DELETED_COLUMN_SIZE = "size";
	
	private SyncOpenHelper openHelper;
	private static SyncDatabaseHelper mInstance = null; 
	private static SQLiteDatabase database;
	
	public SyncDatabaseHelper(Context context) {
		openHelper = new SyncOpenHelper(context);
		database = openHelper.getWritableDatabase();
	}
	
	public static synchronized SyncDatabaseHelper getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new SyncDatabaseHelper(ctx);
        }
        return mInstance;
    }
	
	public void saveProfileRecords(Profile P) {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(Profile_COLUMN_PROFILE, P.getProfileString());
		contentValues.put(Profile_COLUMN_SSID, P.getSsidString());
		contentValues.put(Profile_COLUMN_IP, P.getIpString());
		contentValues.put(Profile_COLUMN_SYNCPATH, P.getSyncpathString());
		contentValues.put(Profile_COLUMN_PORT, P.getPortString());
		
		database.insert(TABLE_NAME_PROFILE, null, contentValues);
	}
	
	public void saveSyncedRecords(FileInfo F) {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(SYNCED_COLUMN_TIMESTAMP, F.getTimeString());
		contentValues.put(SYNCED_COLUMN_NAME, F.getNameString());
		contentValues.put(SYNCED_COLUMN_SIZE, F.getSizeString());
		
		database.insert(TABLE_NAME_SYNCED, null, contentValues);
	}
	
	public void saveDeletedRecords(FileInfo F) {
		ContentValues contentValues = new ContentValues();
		
		contentValues.put(DELETED_COLUMN_TIMESTAMP, F.getTimeString());
		contentValues.put(DELETED_COLUMN_NAME, F.getNameString());
		contentValues.put(DELETED_COLUMN_SIZE, F.getSizeString());
		
		database.insert(TABLE_NAME_DELETED, null, contentValues);
	}
	
	public Cursor getAllProfileRecords() {
		return database.rawQuery(
				"SELECT * FROM " + TABLE_NAME_PROFILE, 
				null);
	}
	
	public Cursor getAllSyncedRecords() {
		return database.rawQuery(
				"SELECT * FROM " + TABLE_NAME_SYNCED, 
				null);
	}
	
	public Cursor getAllDeletedRecords() {
		return database.rawQuery(
				"SELECT * FROM " + TABLE_NAME_DELETED, 
				null);
	}
	
	public static class SyncOpenHelper extends SQLiteOpenHelper {
		
		public SyncOpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database) {
			database.execSQL(
					"CREATE TABLE " + TABLE_NAME_PROFILE + "("
							+ Profile_COLUMN_PROFILE + " TEXT PRIMARY KEY, " 
							+ Profile_COLUMN_SSID + " TEXT, " 
							+ Profile_COLUMN_IP + " TEXT, " 
							+ Profile_COLUMN_PORT + " INTEGER, " 
							+ Profile_COLUMN_SYNCPATH + " TEXT )"
					);
		}

		@Override
		public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
			database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PROFILE + "");
			onCreate(database);
			database.execSQL(
					"CREATE TABLE " + TABLE_NAME_SYNCED + "(" 
							+ SYNCED_COLUMN_TIMESTAMP + " TEXT, " 
							+ SYNCED_COLUMN_NAME + " TEXT, " 
							+ SYNCED_COLUMN_SIZE + " INTEGER )"
					);
			database.execSQL(
					"CREATE TABLE " + TABLE_NAME_DELETED + "(" 
							+ DELETED_COLUMN_TIMESTAMP + " TEXT, " 
							+ DELETED_COLUMN_NAME + " TEXT, " 
							+ DELETED_COLUMN_SIZE + " INTEGER )"
					);
		}
	}
}
