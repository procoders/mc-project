package com.mobilemafia.mytwins.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import support.Metadata.MetaData;

import android.content.Context;
import android.database.Cursor;

public class SyncedList {
	private SyncDatabaseHelper databaseHelper;
	private ArrayList<MetaData> list = new ArrayList<MetaData>(); 
	private MetaData mData;
	private Cursor cursor;
	
	public SyncedList(Context context) {
		databaseHelper = SyncDatabaseHelper.getInstance(context);
	}
	
	public void addRow(String dateStr, String name, long size) throws ParseException {
		FileInfo F = new FileInfo();
		Date date = parseDate(dateStr);
		F.setTimeString(date.toString());
		F.setNameString(name);
		F.setSizeString(String.valueOf(size));
		databaseHelper.saveSyncedRecords(F);
	}
	
	public ArrayList<MetaData> getList() throws ParseException {
		cursor = databaseHelper.getAllSyncedRecords();
		
		if (cursor.moveToFirst()) {
			do {
				mData = new MetaData(cursor.getString(1), cursor.getLong(2), parseDate(cursor.getString(0)), null);
				list.add(mData);
			} while(cursor.moveToNext());
		}
		return list;
	}
	
	public Date parseDate(String dateString) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.parse(dateString);
	}
}
