package com.mobilemafia.mytwins;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.SyncService.SyncFunctionality;
import com.mobilemafia.mytwins.database.SyncDatabaseHelper;

import android.R.integer;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SyncService extends Service 
{
//	public static final String PREFS_NAME = "MyPrefsFile";
	 
	 private String IP;
	 private int port;
	 private String syncFolder;
	 private SyncDatabaseHelper databaseHelper;
	 private Cursor cursor;
	 
	@Override
	public IBinder onBind(Intent arg0) 
	{
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		// Let it continue running until it is stopped.
		Toast.makeText(this, "Sync Service Started", Toast.LENGTH_LONG).show();
		databaseHelper = SyncDatabaseHelper.getInstance(this);
		initialiseData();
		final Handler mHandler = new Handler();
		Runnable runnable = new Runnable() {
	        @Override
	        public void run() {
	        	 startThread();
//	        	if (results == 1){
//	        		mHandler.post(new Runnable() {
//						@Override
//						public void run() {
//							
//						}
//					});
//	        	} else {
//	        		
//	        	}
	        }
	    };
	    new Thread(runnable).start();	    
		return START_STICKY;
	}
	
	private void initialiseData() {
		cursor = databaseHelper.getAllProfileRecords();
		if (cursor.moveToFirst()) {
			IP = cursor.getString(2);
			port = cursor.getInt(3);
			syncFolder = cursor.getString(4);
		}
	
		Toast.makeText(this, "ABHISHEK "+IP+" "+port+" "+syncFolder, Toast.LENGTH_LONG).show();
		
	}

	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		Toast.makeText(this, "Sync Service Destroyed", Toast.LENGTH_LONG).show();
   }
	
	public static String getDataDir(Context context) throws Exception {
	    return context.getPackageManager()
	            .getPackageInfo(context.getPackageName(), 0)
	            .applicationInfo.dataDir;
	}
	


	
	static byte[] toByteArray(int value) {
	    return new byte[] {
	        (byte) (value >> 24),
	        (byte) (value >> 16),
	        (byte) (value >> 8),
	        (byte) value};
	}
	
	static int fromByteArray(byte[] bytes) {
	     return bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
	}
	
	public static void startThread()
	 {
		 Thread t=new Thread(new SyncFunctionality());
		 t.start();
	 }
}
