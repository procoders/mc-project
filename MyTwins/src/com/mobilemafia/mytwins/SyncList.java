package com.mobilemafia.mytwins;

import java.util.Arrays;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SyncList extends ListActivity {
	
	public static Object[] Songs = {
		"MySong", "Your Beats", "Sample", "Tring", "Too", "Stood", "Now",
		"Have It", "This is good", "all is well", "Now you can do it", "Great",
		"Common do it", "My Dreams", "State the value", "All is well here"
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sync_list);
		
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.listrow, Songs);
		setListAdapter(adapter);
		
		mList = (TouchInterceptor) getListView();
		mList.setDropListener(mDropListener);
		registerForContextMenu(mList);
	}
		
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String selection = Songs[position].toString();
		Toast.makeText(this, selection, Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return false;
	}
	
	private TouchInterceptor mList;
	
	private TouchInterceptor.DropListener mDropListener =
			new TouchInterceptor.DropListener() {
				public void drop(int from, int to) {

					System.out.println("Droplisten from: " + from + " to: " + to);
	
					//Assuming that item is moved up the list	
					int direction = -1;
					int loop_start = from;
					int loop_end = to;
	
					//For instance where the item is dragged down the list	
					if(from < to) {
						direction = 1;
					}
	
					Object target = Songs[from];	
					for(int i = loop_start; i != loop_end; i = i+direction){
						Songs[i] = Songs[i+direction];
					}	
					Songs[to] = target;	
					
					System.out.println("Changed array is: " + Arrays.toString(Songs));
		
					((BaseAdapter) mList.getAdapter()).notifyDataSetChanged();
				}
			};
}
