package com.mobilemafia.mytwins;

import com.mobilemafia.mytwins.database.Profile;
import com.mobilemafia.mytwins.database.SyncDatabaseHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MakeProfile extends Activity {
	private static final int PICKFILE_RESULT_CODE = 1;
	TextView editFile,editssid;
	private WifiManager wifiManager;
	private Profile P = new Profile();
	private SyncDatabaseHelper databaseHelper;
	private boolean fieldsOK;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_make_profile);
		databaseHelper = SyncDatabaseHelper.getInstance(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.make_profile, menu);
		return true;
	}
	  public void folder_path(View arg0) {

    	   Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    	            intent.setType("file/*");
    	      startActivityForResult(intent,PICKFILE_RESULT_CODE);
    	  
    	  }
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		 switch(requestCode){
		 case PICKFILE_RESULT_CODE:
		  if(resultCode==RESULT_OK){
			  
			  String FilePath = data.getData().getPath();
			  String FileName = data.getData().getLastPathSegment();
			  int lastPos = FilePath.length() - FileName.length();
			  String Folder = FilePath.substring(0, lastPos);
			  editFile = (TextView)findViewById(R.id.i_FolderPath);
			  editFile.setText(Folder);
		  
		  }
		  break;
		 
		 }
		}
	
	public void GoToProgress(View v)
	{
		TextView profile, ip, port, ssid, syncpath;
		profile = (TextView)findViewById(R.id.i_profilename);
		ip = (TextView)findViewById(R.id.i_ipAddress);
		ssid = (TextView)findViewById(R.id.i_EnterSSID);
		port = (TextView)findViewById(R.id.i_port);
		syncpath = (TextView)findViewById(R.id.i_FolderPath);
		String message = ""+syncpath.getText()+ssid.getText()+profile.getText()+ip.getText()+port.getText();
		
    	
		fieldsOK = validate(new TextView[]{profile, ip, ssid, port, syncpath});
		if (fieldsOK == true) {
	    	P.setProfileString(profile.getText().toString());
	    	P.setSsidString(ssid.getText().toString());
	    	P.setIpString(ip.getText().toString());
	    	P.setSyncpathString(syncpath.getText().toString());
	    	P.setPortString(port.getText().toString());
	    	databaseHelper.saveProfileRecords(P);
	    
	    	int duration = Toast.LENGTH_SHORT;
			Toast toast = Toast.makeText(this, "Profile " + P.getProfileString() + " created", duration);
			toast.show();
			
			Intent i = new Intent(getApplicationContext(), MainScreen.class);
	    	this.startActivity(i);
		} else {
			Toast toast = Toast.makeText(this, "Incomplete Profile not an option!!!", Toast.LENGTH_LONG);
			toast.show();
		}
	}
	
	private boolean validate(TextView[] fields) {
		for(int i = 0; i < fields.length; i++){
            TextView currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                return false;
            }
        }
        return true;
	}

	//Method to get SSID
    public void getSSID(View view)
    {
    	//get context
    	Context context = getApplicationContext();
  	  	
    	CharSequence text;
    	
  	  	//getssid
    	wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    	if(wifiManager.isWifiEnabled())
    	{
  	  		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
  	  		text = wifiInfo.getSSID();
  	  		text=text.toString().substring(1, text.length()-1);
  	  		System.out.println(text);
	  	  	editssid  = (TextView)findViewById(R.id.i_EnterSSID);
	    	editssid.setText(text);
    	}
    	else
    	{
    		text ="Wifi is disabled.";
    	
  	  	//popup
    	//CharSequence text = "Hello toast!";
    		int duration = Toast.LENGTH_LONG;
    		Toast toast = Toast.makeText(context, text, duration);
    		toast.show();
    		
    	}
    }
    
	
}
