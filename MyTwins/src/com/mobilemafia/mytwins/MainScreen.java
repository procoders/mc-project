package com.mobilemafia.mytwins;

import java.util.ArrayList;

import com.SyncService.SyncFunctionality;
import com.mobilemafia.mytwins.database.SyncDatabaseHelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

public class MainScreen extends Activity {
	
	static Thread t = null;

	public ProgressBar syncProg;
	public String runState;
	public Switch startStop;
	ImageButton syncButton;
	public TextView StatusText;
	boolean serviceRunning = false;
	boolean frontsyncRunning = false;
	public final Context cContext=this; 
	private SyncDatabaseHelper databaseHelper;
	private Cursor cursor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		databaseHelper = SyncDatabaseHelper.getInstance(cContext);
		cursor = databaseHelper.getAllProfileRecords();
		
		final ListView listview = (ListView) findViewById(R.id.listView1);
		
		final ArrayList<String> list = new ArrayList<String>();
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getString(0));
			} while(cursor.moveToNext());
		}
	    
	    final StableArrayAdapter adapter = new StableArrayAdapter(this,android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
		    
		syncButton = (ImageButton) findViewById(R.id.forceSyncButton);
//		syncButton.setOnClickListener(new OnClickListener() {
//			 
//			@Override
//			public void onClick(View v) {
////				Intent intent = new Intent(getBaseContext(), SyncService.class);
////				startActivity(intent);
//				
//				if (serviceRunning==false)
//				{
//					syncButton.setImageResource(R.drawable.redstopsync);
//					startService(new Intent(getBaseContext(), SyncService.class));
//			    	serviceRunning=true;
//			    	setprogressValue(20);
//				}
//				else
//				{
//					syncButton.setImageResource(R.drawable.greenync);
//					stopService(new Intent(getBaseContext(), SyncService.class));
//					serviceRunning=false;
//				}			    
//			}
//		});
		
		
		syncProg=(ProgressBar) findViewById(R.id.appProgress);
//		startStop=(Switch) findViewById(R.id.startStop);
//		runState=(String)startStop.getText();
//		startStop.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			private boolean serviceRunning;
//
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//			    Log.v("Switch State=", ""+runState);
//			    if(isChecked)
//			    {
//			    	startService(new Intent(getBaseContext(), SyncService.class));
//			    	serviceRunning=true;
//			    	setprogressValue(20);
//			    }
//			    else
//			    {
//			    	stopService(new Intent(getBaseContext(), SyncService.class));
//			    	serviceRunning=false;
//			    	setprogressValue(0);
//			    }
//			}       
//			});
		setprogressValue(20);
		startStop=(Switch) findViewById(R.id.startStop);
		startStop.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			private boolean serviceRunning;
			

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				// TODO Auto-generated method stub
				Log.v("Switch State=", ""+runState);
			    if(isChecked)
			    {
			    	startService(new Intent(getBaseContext(), SyncService.class));
			    	serviceRunning=true;
			    	setprogressValue(20);

			    }
			    else
			    {
			    	stopService(new Intent(getBaseContext(), SyncService.class));
			    	serviceRunning=false;
			    	setprogressValue(0);
			    }
			}       
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_screen, menu);
		return true;
		
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
            case R.id.PrioritizeSync:
            	//if (serviceRunning)
            	Intent i = new Intent(getApplicationContext(), SyncList.class);
            	this.startActivity(i);
                return true;
            case R.id.CreateProfile:
            	Intent i1 = new Intent(getApplicationContext(), MakeProfile.class);
            	this.startActivity(i1);
                return true;
            case R.id.Home:
            	Intent i2 = new Intent(getApplicationContext(), MainScreen.class);
            	this.startActivity(i2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
    	}
    }
	
	
	public void setStatusText(boolean isSync)
	{
		if (isSync == true)
		{
			StatusText.setText(R.string.setStringtoSync);
		}
		else
		{
			StatusText.setText(R.string.setStringtoWait);
		}
	}
	
	public void setSyncFilename(String fileSyncing)
	{
		
	}
	
	
	public void setprogressValue(int pValue) 
	{
		final float[] roundedCorners = new float[] { 5, 5, 5, 5, 5, 5, 5, 5 };
		ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null,null));
		//String MyColor = "#1AD6EB";
		String MyColor="#4274CB"; 
		pgDrawable.getPaint().setColor(Color.parseColor(MyColor));
		ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
		syncProg.setProgressDrawable(progress);   
		syncProg.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
		syncProg.setProgress(pValue);
	}
	
	public void ForceSync(View arg0)
	 {		 
		 startThread();
		if (serviceRunning==false)
		{
			syncButton.setImageResource(R.drawable.redstopsync);
//			startService(new Intent(getBaseContext(), SyncService.class));
	    	serviceRunning=true;
//	    	setprogressValue(20);
		}
		else
		{
			syncButton.setImageResource(R.drawable.greenync);
//			stopService(new Intent(getBaseContext(), SyncService.class));
			serviceRunning=false;
		}	
	 }
	
	 public static void startThread()
	 {
		 t=new Thread(new SyncFunctionality());
		 t.start();
	 }
}
