package com.mobilemafia.mytwins;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
//
public class ProgressActivity extends Activity {
    
	public ProgressBar syncProg;
	public String runState;
	public Switch startStop;
	public TextView StatusText;
	private Button syncButton;
	private final Context pContext = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.progress_activity);
		StatusText=(TextView) findViewById(R.id.syncStatus);
		syncProg=(ProgressBar) findViewById(R.id.appProgress);
		startStop=(Switch) findViewById(R.id.startStop);
		runState=(String)startStop.getText();
		startStop.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			private boolean serviceRunning;

			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			    Log.v("Switch State=", ""+runState);
			    if(isChecked)
			    {
			    	startService(new Intent(getBaseContext(), SyncService.class));
			    	serviceRunning=true;
			    	setprogressValue(20);
//			    	for(int i=0;i<10000000;i=i+)
//					{
//						if(i%100000==0)
//							setprogressValue(i/1000000);
//						try {
//							Thread.sleep(100);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//							
//					}
			    }
			    else
			    {
			    	stopService(new Intent(getBaseContext(), SyncService.class));
			    	serviceRunning=false;
			    	setprogressValue(0);
			    }
			}       
			});
		setprogressValue(0);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void setStatusText(boolean isSync)
	{
		if (isSync == true)
		{
			StatusText.setText(R.string.setStringtoSync);
		}
		else
		{
			StatusText.setText(R.string.setStringtoWait);
		}
	}
	
	public void setSyncFilename(String fileSyncing)
	{
		
	}
	
	public void setprogressValue(int pValue) 
	{
		final float[] roundedCorners = new float[] { 5, 5, 5, 5, 5, 5, 5, 5 };
		ShapeDrawable pgDrawable = new ShapeDrawable(new RoundRectShape(roundedCorners, null,null));
		String MyColor = "#1AD6EB";
		pgDrawable.getPaint().setColor(Color.parseColor(MyColor));
		ClipDrawable progress = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);
		syncProg.setProgressDrawable(progress);   
		syncProg.setBackgroundDrawable(getResources().getDrawable(android.R.drawable.progress_horizontal));
		syncProg.setProgress(pValue);
	}

}
