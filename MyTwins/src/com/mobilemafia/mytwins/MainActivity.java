package com.mobilemafia.mytwins;

import com.mobilemafia.mytwins.database.SyncDatabaseHelper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private WifiManager wifiManager;
	public static Context myContext;
	private static SyncDatabaseHelper databaseHelper;
	private Cursor cursor;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseHelper = SyncDatabaseHelper.getInstance(this);
        cursor = databaseHelper.getAllProfileRecords();
        
        if(cursor.getCount() > 0)
        {
        	Intent i = new Intent(getApplicationContext(), MainScreen.class);
	    	this.startActivity(i);
        }
        else
        {	
        	setContentView(R.layout.no_profile);
        	Button button = (Button) findViewById(R.id.profile);
        	button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent i = new Intent(getApplicationContext(), MakeProfile.class);
	            	startActivity(i); 
				}
			});
        }
        getActionBar().setDisplayHomeAsUpEnabled(true);
        myContext=this;
    }
    
    // Method to start the service
    public void startService(View view)
    {
       startService(new Intent(getBaseContext(), SyncService.class));
    }

    // Method to stop the service
    public void stopService(View view) 
    {
       stopService(new Intent(getBaseContext(), SyncService.class));
    }
    
    //Method to toggle Wifi
    public void toggleWifi(View view)
    {
    	//get context
    	Context context = getApplicationContext();
    	
    	//toggle wifi
    	wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
  	  	if(wifiManager.isWifiEnabled())
  	  	{
  	  		wifiManager.setWifiEnabled(false);
  	  	}
  	  	else
  	  	{
  	  		wifiManager.setWifiEnabled(true);
  	  	}
    }
    
    //Method to get SSID
    public void getSSID(View view)
    {
    	//get context
    	Context context = getApplicationContext();
  	  	
    	CharSequence text;
    	
  	  	//getssid
    	wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    	if(wifiManager.isWifiEnabled())
    	{
  	  		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
  	  		text = wifiInfo.getSSID();
    	}
    	else
    	{
    		text ="Wifi is disabled.";
    	}
  	  	//popup
    	//CharSequence text = "Hello toast!";
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
            case R.id.CreateProfile:
            	Intent i = new Intent(getApplicationContext(), MakeProfile.class);
            	this.startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
    	}
    }
}
