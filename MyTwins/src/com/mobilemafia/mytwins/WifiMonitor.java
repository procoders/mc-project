package com.mobilemafia.mytwins;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import com.mobilemafia.mytwins.database.SyncDatabaseHelper;

public class WifiMonitor extends BroadcastReceiver {
	
	private WifiManager wifiManager;
	private static Context context;
	private SyncDatabaseHelper databaseHelper;
	private Cursor cursor;
	private String SSID;
	
	public WifiMonitor() {
		super();
	}

    @Override
    public void onReceive(Context context, Intent intent) {
        
    	WifiMonitor.context=context;
 	   	
    	//here, check that the network connection is available. If yes, start your service. If not, stop your service.
       ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo info = cm.getActiveNetworkInfo();
       databaseHelper = SyncDatabaseHelper.getInstance(context);
       cursor = databaseHelper.getAllProfileRecords();
		if (cursor.moveToFirst()) {
			SSID = cursor.getString(1);
		}
       
       if (info != null) 	//check
       {
           if (info.isConnected()) 
           {
        	   if(getSSID().equals(SSID));
        	   	startSyncService();
           }
           else 
           {
//        	   CharSequence text="Wifi turned off 1";
//        	   int duration = Toast.LENGTH_LONG;
//        	   Toast.makeText(context, text, duration).show();
//        	   stopSyncService();
           }
       }
       else
       {
    	   CharSequence text="Wifi turned off 2";
    	   int duration = Toast.LENGTH_LONG;
    	   Toast.makeText(context, text, duration).show();
    	   stopSyncService();
       }
    }
    
  private void stopSyncService() 
  {
	  Intent intent = new Intent(context, SyncService.class);
	  context.stopService(intent);
  }

  private void startSyncService() 
  {
	  Intent intent = new Intent(context, SyncService.class);
	  context.startService(intent);	
  }

	//Method to get SSID
    public String getSSID()
    { 	
    	CharSequence text;
    	
  	  	//getssid
    	wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    	if(wifiManager.isWifiEnabled())
    	{
  	  		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
  	  		text = wifiInfo.getSSID();
    	}
    	else
    	{
    		text ="Wifi is disabled.";
    	}
    	
    	int duration = Toast.LENGTH_LONG;
    	Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
    	
    	return text.toString();
  	  	//popup
    	//CharSequence text = "Hello toast!";
    	
    }
}