package com.SyncService;

import help.subWorker.SubWorkerReceiver;
import help.subWorker.SubWorkerSender;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import error.handler.ErrorAPI;
import error.handler.ErrorServer;
import support.Metadata.MetaData;
import support.Metadata.MetaDataList;
import support.Protocol.Signal;
import support.SendReceive.SendReceiveAPI;
import support.SendReceive.Server;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

public class SyncFunctionality implements Runnable{
	
	MetaDataList mobileMetadata=null;
	MetaDataList desktopMetadata=null;
	SendReceiveAPI sendReceiveAPI=null;
	private int sleepMin=0;
	Server server=null;
	TextView message=null;
	ErrorServer errorServer=null;

	//sstatic int tag=1;
	@Override
	public void run() 
	{
		int i=0;
		while(true)
		{
//			i++;
//			System.out.println("************************");
//			System.out.println(i);
//			System.out.println("************************");
			
			SyncFunctionality();
			
			try 
			{
				TimeUnit.MINUTES.sleep(sleepMin);
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
		}
	
    }
	
	private void SyncFunctionality()
	{
		try
		{
//			errorServer=new ErrorServer();
//			ErrorAPI ea=new ErrorAPI(errorServer);
//	    	ea.sendErorr();
	    	server=new Server();
			sendReceiveAPI=new SendReceiveAPI(server);
			
	    	Connect();
//	    	if(SyncFunctionality.tag==1)
//	    	{
//	    		SyncFunctionality.tag=2;
//	    		fakeException();
//	    	}
	    	sendReceiveAPI.senderDemoText(Signal.REQUEST_DIR);
			 
			desktopMetadata=(MetaDataList) sendReceiveAPI.receiverDemoText("l");
			int results = saveMetadata();
	    	if (results == 1)
	    	{
				Log.d("META", "Metadata Saved");
	    	} 
	    	else 
	    	{
	    		Log.d("META ", "Metadata Not Saved");
	    	}
	    	
	    	//System.out.println(mobileMetadata);
	    	mobileMetadata.diff(desktopMetadata);
	    	//System.out.println(mobileMetadata);
	    	//System.out.println(desktopMetadata);
	    	sendReceiveAPI.senderDemoText(mobileMetadata);
			sendReceiveAPI.senderDemoText(desktopMetadata);
			
			receiveSignal(Signal.READY_SERV);
			
			sendReceiveAPI.senderDemoText(Signal.READY_APP);
			startSubWorker1();
			receiveSignal(Signal.DONE_SERV);
			
			sendReceiveAPI.senderDemoText(Signal.CONFIRM_FORM_APP);
			startSubWorker2();
			
			receiveSignal(Signal.ACCEPT_FORM_SERV);
			sendReceiveAPI.senderDemoText(Signal.DONE_APP);
			receiveSignal(Signal.CONFIRM_FROM_SERV);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
		catch (Throwable t)
		 {
			 System.out.println("Interrupted : throw 2");
		 }
		finally
		{
			server.closeServer();
			//errorServer.closeServer();
		}
	}
	
	private void Connect()
	 {
		 while(true)
		 {
			//sendReceiveAPI.senderDemoText(Signal.HELLO_SERV);
			if(sendReceiveAPI.receiverDemoText("sig").equals(Signal.HELLO_APP))
			{
				System.out.println("CONNECTED");
				break;
			}
			
//			try 
//			{
//				TimeUnit.MINUTES.sleep(sleepMin);
//			} 
//			catch (InterruptedException e) 
//			{
//				e.printStackTrace();
//			}
			
		 }
			
	 }
	 
	 private void startSubWorker2() 
	 {
//		 Runnable runnable = new Runnable()
//		 {
//		        @Override
//		        public void run() 
//		        {
					 SubWorkerSender subWorkerSender=new SubWorkerSender();
					 subWorkerSender.sendMusicFiles(mobileMetadata,sendReceiveAPI);
//		        }
//		 };
//		 
//		 new Thread(runnable).start();
	 }
	 
	 private void startSubWorker1()
	 {
//		 Runnable runnable = new Runnable()
//		 {
//		        @Override
//		        public void run() 
//		        {
					 try
					 {
						SubWorkerReceiver.startReceiver(desktopMetadata,sendReceiveAPI);
					 } 
					 catch (IOException e) 
					 {
						e.printStackTrace();
					 }
//		        }
//		 };
//		 new Thread(runnable).start();        
	 }
	 	 
	 public int saveMetadata()
	 {
		    File file = getSyncDir();
		    File list[] = file.listFiles();
		    
		    mobileMetadata=new MetaDataList();
		    
		    for(File mp3:list)
		    {	
		    	if(mp3.getName().matches(".*\\.mp3$"))
		    	{
		    		MetaData metaData=new MetaData(mp3.getName(), mp3.length(), new Date(mp3.lastModified()),mp3.getAbsolutePath());
		    		mobileMetadata.list.add(metaData);
		    	}
		    }
		    Log.d("MetaDataList",mobileMetadata.toString());
		    return 1;
	}

	private File getSyncDir() 
	{
		String root_sd = Environment.getExternalStorageDirectory().toString();
	    return new File( root_sd + "/downloads/bluetooth" ) ;
	    
	}
	
	private void receiveSignal(Signal signal) {
		while(true)
		{
			if(sendReceiveAPI.receiverDemoText("sig")==signal)
			{
				break;
			}
				
		}
		
	}
}
