package error.handler;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import support.Protocol.IPandPort;

public class ErrorAPI 
{
	ErrorServer server=null;
	private int receivePort=IPandPort.getPort(1);
	private String receiveIP=IPandPort.getDesktopIP();
	
	public ErrorAPI(ErrorServer es)
	{
		this.server=es;	
	}
	
	 public void sendErorr()
	 { 
		printMessage("Error","Sending Error");
		try
		{
			SocketChannel sChannel = SocketChannel.open();
			sChannel.configureBlocking(true);
			sChannel.connect(new InetSocketAddress(receiveIP, receivePort));
			sChannel.close();
		}
		catch(Exception e){e.printStackTrace();}
		
		printMessage("Error","Error Sent");
	 }
	 
	 public void ReceiveError() 
	 {
		printMessage("Error","Listining for Error");
		try
		{
			server.getSsChannel().accept(); //blocking
		}
		catch(Exception e){e.printStackTrace();}
		printMessage("Error","Error Restarting");
		
	 }
	 
	 void printMessage(String tag,String msg)
	 {
		 System.out.println(tag+" : "+msg);
	 }
}
