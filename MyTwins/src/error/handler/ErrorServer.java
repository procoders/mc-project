package error.handler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import support.Protocol.IPandPort;

public class ErrorServer 
{
	//sender is server
	private int sendPort=IPandPort.getPort(0);
	private SocketChannel sChannel=null;
	private ServerSocketChannel ssChannel=null;
	
	public ErrorServer() throws IOException
	{
		sendStartup();
	}
	
	private void sendStartup() throws IOException
	{
		ssChannel = ServerSocketChannel.open();
		ssChannel.configureBlocking(true);
		ssChannel.socket().bind(new InetSocketAddress(sendPort));
	}
	
	public void closeServer() 
	{
		try
		{
			if(sChannel!=null)
				if(sChannel.isOpen())
				{
				sChannel.close();
				}
		}catch(Exception e){e.printStackTrace();}
			
		try
		{
			if(ssChannel!=null)
			if(ssChannel.isOpen())
			{
				ssChannel.close();
			}
		}catch(Exception e){e.printStackTrace();}	
		
		sChannel=null;
		ssChannel=null;
	}

	public SocketChannel getsChannel() {
		return sChannel;
	}

	public void setsChannel(SocketChannel sChannel) {
		this.sChannel = sChannel;
	}

	public int getSendPort() {
		return sendPort;
	}

	public ServerSocketChannel getSsChannel() {
		return ssChannel;
	}

}
